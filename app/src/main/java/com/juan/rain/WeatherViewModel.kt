package com.juan.rain

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.juan.rain.data.WeatherRepository
import com.juan.rain.model.LatLng
import com.juan.rain.model.Weather

class WeatherViewModel(val repository: WeatherRepository): ViewModel() {

    /** TODO
     * When asked for weather get the date and aproximate the hour we are in
     * by a 3 hour margin starting from 00
     * Then ask to the repository for it
     */
    fun getWeather(location:LatLng) = repository.getWeather(location)
}

class WeatherFactory(val repository: WeatherRepository):ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WeatherViewModel(repository) as T
    }
}