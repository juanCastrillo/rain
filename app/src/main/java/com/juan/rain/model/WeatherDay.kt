package com.juan.rain.model

import androidx.room.Entity
import org.threeten.bp.LocalDate

@Entity
data class WeatherDay(
    var date: LocalDate
)