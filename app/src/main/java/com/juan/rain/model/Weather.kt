package com.juan.rain.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalTime

@Entity
data class Weather(
    @PrimaryKey var id:Long?,
    var date: LocalDate,
    var hour:Int,
    var summary:String,
    @Embedded var precipitation:Precipitation,
    //@Embedded var precipitationFuture:MutableList<MinutePrecipitation>,
    @Embedded var temperature:Temperature,
    @Embedded var wind:Wind
)

data class MinutePrecipitation (
    var time: LocalTime,
    var precipPerc:Double
)

data class Wind(
    var angle:Double,
    var speed:Double
)

data class Temperature(
    //var min_temp:Double,
    var avg_temp:Double,
    //var max_temp:Double
    var sens_temp:Double
)

data class Precipitation(
    var cloudCover: Double,
    var percentage:Double,
    var intensity:Double
    //var precipitationType:PrecipitationType
)

enum class PrecipitationType {
    SNOW,
    RAIN
}

