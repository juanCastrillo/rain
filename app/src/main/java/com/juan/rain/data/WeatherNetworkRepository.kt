package com.juan.rain.data

import android.content.Context
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.os.ConfigurationCompat
import com.juan.rain.model.*
import com.juan.rain.parse.WeatherParse


class WeatherNetworkRepository(context: Context) {

    val queue = Volley.newRequestQueue(context)
    var app = context.packageManager.getApplicationInfo(context.packageName, PackageManager.GET_META_DATA)
    var bundle = app.metaData
    val key = bundle.getString("DarkSky_key")

    //TODO allow users to choose units
    val units = "si"
    val exclusions = "currently,daily,flags"
    val currentLocale = ConfigurationCompat.getLocales(context.resources.configuration)[0].language

    fun placeUrl(location: LatLng) = "$baseUrl/$key/${location.latitude},${location.longuitude}?" +
            "lang=$currentLocale" + "&units=$units" + "&exclude=$exclusions" //+ "&extend=hourly"

    private var weather:List<Weather>? = listOf()

    fun weatherRequest(location: LatLng, callBack:WeatherCallback) {
        val url = placeUrl(location)
        Log.d(TAG, "url: $url")
        val request = StringRequest(
            Request.Method.GET, url,
            Response.Listener<String> { response ->
                weather = (WeatherParse.parseDarkSky(response))
                callBack.onResponse(weather)
            },
            Response.ErrorListener { error -> callBack.onError(error.toString())}
        )
        queue.add(request)
    }

    private fun parseOpenWeather(response: String?): Weather? {
        val json = JSONObject(response)
        val weatherlist = JSONArray(json["list"])
        val size = weatherlist.length()
        for(i in 0 until size){}
        return null
    }

    companion object {
        private val TAG = "WeatherNetworkRepo"
        val baseUrl = "https://api.darksky.net/forecast"
    }
}