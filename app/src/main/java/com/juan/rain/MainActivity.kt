package com.juan.rain

import android.animation.ObjectAnimator
import android.graphics.Outline
import android.graphics.Rect
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.juan.rain.model.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var bottomBox: View
    private lateinit var windBox:View
    private lateinit var rainBox:View
    private lateinit var percentageRainText: TextView
    private lateinit var animationUtils:AnimationUtils

    private lateinit var rainViewModel: WeatherViewModel

    var percentageRain = 0
    var once = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rainViewModel = ViewModelProviders.of(this, WeatherFactory((application as Start).repository))
            .get(WeatherViewModel::class.java)

        animationUtils = AnimationUtils(resources)

        rainBox = findViewById(R.id.rainBox)
        windBox = findViewById(R.id.windBox)
        bottomBox = findViewById(R.id.bottomBox)
        percentageRainText = findViewById(R.id.percentText)

        rainViewModel.getWeather(LatLng(56.4689,-35.8268)).observe(this, Observer {
            if(it.isNotEmpty()) updateUi(it)
        })

        startAnimations()

    }

    private fun updateUi(weather:List<Weather>) {
        percentageRain = (weather[0].precipitation.percentage*100).toInt()
        percentageRainText.text = percentageRain.toString() +" %"
        weatherDescriptionText.text = weather[0].summary
        windSpeedText.text = weather[0].wind.speed.toString() + " m/s"
        windCompassDirectionText.text = angleToCompassDirection(weather[0].wind.angle).toLocalizedString(this)

        //rainBox.viewTreeObserver.addOnDrawListener {
            //Filling the background with blue to represent the percentage of rain
            val height =  rainBox.measuredHeight.toFloat()
            //Log.d("MainActivity", "height: $height")
            if(once) { once = false
                rainBox.pivotY = height
                ObjectAnimator.ofFloat(rainBox, "scaleY", percentageRain/100f).apply {
                    duration = 1000
                    start()
                }
            }
        //}
    }

    private fun startAnimations() {
        //Bottom box entrance from the left appearing
        ObjectAnimator.ofFloat(bottomBox, "translationX",
            animationUtils.dpToPx(0f)).apply {
            duration = 1000
            start()
        }

        ObjectAnimator.ofFloat(windBox, "translationX",
            animationUtils.dpToPx(0f)).apply {
            duration = 1000
            start()
        }


    }
}
