package com.juan.rain.parse

import com.juan.rain.model.Precipitation
import com.juan.rain.model.Temperature
import com.juan.rain.model.Weather
import com.juan.rain.model.Wind
import com.juan.rain.utils.DateTimeUtils
import org.json.JSONArray
import org.json.JSONObject

class WeatherParse {
    companion object {

        fun parseDarkSky(response: String?): List<Weather>? {
            if (response == null) return null
            val weatherList = mutableListOf<Weather>()

            val json = JSONObject(response)
            val weather = json.get("hourly") as JSONObject
            val hourlyWeatherList = weather.get("data") as JSONArray

            val size = hourlyWeatherList.length()
            for (i in 0 until size) {
                val hourWeatherJsonObject = hourlyWeatherList[i] as JSONObject

                val dateTime = DateTimeUtils.epochToLocalDateTime((hourWeatherJsonObject["time"] as Int).toLong())

                weatherList.add(
                    Weather(
                        null,
                        date = dateTime.toLocalDate(),
                        hour = dateTime.hour,
                        summary = hourWeatherJsonObject["summary"] as String,
                        precipitation = Precipitation(
                            percentage = hourWeatherJsonObject["precipProbability"].toString().toDouble(),
                            intensity = hourWeatherJsonObject["precipIntensity"].toString().toDouble(),
                            cloudCover = hourWeatherJsonObject["cloudCover"].toString().toDouble()
                        ),
                        temperature = Temperature(
                            avg_temp = hourWeatherJsonObject["temperature"].toString().toDouble(),
                            sens_temp = hourWeatherJsonObject["apparentTemperature"].toString().toDouble()
                        ),
                        wind = Wind(
                            angle = hourWeatherJsonObject["windBearing"].toString().toDouble(),
                            speed = hourWeatherJsonObject["windSpeed"].toString().toDouble()
                        )
                    )
                )
            }
            return weatherList
        }
    }
}