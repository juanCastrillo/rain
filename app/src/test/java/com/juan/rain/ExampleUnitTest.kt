package com.juan.rain

import com.juan.rain.api.Tools.Companion.sunriseTime
import com.juan.rain.model.CompassDirections
import com.juan.rain.model.angleToCompassDirection
import com.juan.rain.utils.DateTimeUtils
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.time.LocalDate

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(RobolectricTestRunner::class)
class ExampleUnitTest {
    @Test
    fun compassDirectionIsOkay() {
        val degree = 180-22.5F
        val expected = CompassDirections.SOUTH
        val result = angleToCompassDirection(degree)
        assertEquals(expected, result)
    }

    @Test
    fun getSunriseCool(){
        val sunrise = sunriseTime()
        val expected = "idk"
        assertEquals(expected, sunrise)
    }

    @Test
    fun epochToDate(){
        val expected = LocalDate.of(2019, 2, 22)
        val result = DateTimeUtils.epochToLocalDateTime(1550839320)

        assertEquals(expected, result)
    }
}
