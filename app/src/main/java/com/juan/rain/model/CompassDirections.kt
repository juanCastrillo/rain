package com.juan.rain.model

import android.content.Context
import com.juan.rain.R
import com.juan.rain.model.CompassDirections.*

enum class CompassDirections {
    NORTH,
    SOUTH,
    EAST,
    WEST,
    NORTH_EAST,
    NORTH_WEST,
    SOUTH_EAST,
    SOUTH_WEST
}

fun CompassDirections.toLocalizedString(context: Context) =
    when(this){
        NORTH -> context.resources.getString(R.string.north)
        SOUTH -> context.resources.getString(R.string.south)
        EAST -> context.resources.getString(R.string.east)
        WEST -> context.resources.getString(R.string.west)
        NORTH_EAST -> context.resources.getString(R.string.north_east)
        NORTH_WEST -> context.resources.getString(R.string.north_west)
        SOUTH_EAST -> context.resources.getString(R.string.south_east)
        SOUTH_WEST -> context.resources.getString(R.string.south_west)
    }

/**
 * Given an angle rom 0 to 360 calculates the compass direction
 * given priority to non combine ones ex. NORTH > NORTH_EAST when on borders like 22.5
 */
fun angleToCompassDirection(angle:Double) =
    when{
        angle in (360-22.5)..360.0 || angle in 0.0..22.5 -> NORTH
        angle in range(90)  -> EAST
        angle in range(180) -> SOUTH
        angle in range(270) -> WEST
        angle in range(45)  -> NORTH_EAST
        angle in range(135) -> SOUTH_EAST
        angle in range(225) -> SOUTH_WEST
        angle in range(315) -> NORTH_WEST
        else -> NORTH
    }

fun range(angle:Int):ClosedRange<Double> =  angle-22.5..angle+22.5