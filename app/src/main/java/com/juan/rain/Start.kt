package com.juan.rain

import android.app.Application
import androidx.room.Room
import com.jakewharton.threetenabp.AndroidThreeTen
import com.juan.rain.data.WeatherNetworkRepository
import com.juan.rain.data.WeatherRepository
import com.juan.rain.storage.database.WeatherDatabase

class Start: Application() {
    lateinit var repository: WeatherRepository
    private lateinit var networkRepository: WeatherNetworkRepository
    lateinit var database: WeatherDatabase

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        networkRepository = WeatherNetworkRepository(this)
        database = WeatherDatabase.getInstance(this)
        repository = WeatherRepository(database.weatherDao(), networkRepository)

    }
}