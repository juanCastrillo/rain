package com.juan.rain.storage.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.juan.rain.model.Weather
import org.threeten.bp.LocalDate

@Dao
interface WeatherDao {

    @Query("SELECT * FROM weather WHERE date = :date and hour >= :hour")
    fun getWeather(date:LocalDate, hour:Int):LiveData<List<Weather>>

    @Query("SELECT weather.id FROM weather WHERE date = :date and hour = :hour")
    fun checkAvailability(date:LocalDate, hour:Int):Long?

    @Update
    fun updateWeather(weather:Weather)

    @Insert
    fun insertWeather(weather:List<Weather>)
}
