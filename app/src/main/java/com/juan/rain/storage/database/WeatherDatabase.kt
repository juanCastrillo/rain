package com.juan.rain.storage.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.juan.rain.model.Weather
import com.juan.rain.storage.converters.LocalDateConverters
import com.juan.rain.storage.dao.WeatherDao

@Database(entities = [Weather::class], version = 1, exportSchema = false)
@TypeConverters(LocalDateConverters::class)
abstract class WeatherDatabase:RoomDatabase() {
    abstract fun weatherDao():WeatherDao

    companion object {
        private var INSTANCE: WeatherDatabase? = null
        fun getInstance(ct: Context): WeatherDatabase {
            if (INSTANCE == null) {
                synchronized(WeatherDatabase::class.java) {
                    INSTANCE = Room.databaseBuilder(
                        ct.applicationContext, WeatherDatabase::class.java, "weather_database.db"
                    ).build()
                }
            }
            return INSTANCE!!
        }

        fun destroyInstance(){ INSTANCE = null }
    }
}