package com.juan.rain.storage.converters

import androidx.room.TypeConverter
import com.juan.rain.utils.stringToLocalDate
import com.juan.rain.utils.toArchiveString
import org.threeten.bp.LocalDate

class LocalDateConverters{
    @TypeConverter
    fun localDateToStringConverter(date: LocalDate):String = date.toArchiveString()

    @TypeConverter
    fun stringToLocalDateConverter(date:String):LocalDate = stringToLocalDate(date)
}