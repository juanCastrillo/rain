package com.juan.rain

import android.content.res.Resources
import android.util.DisplayMetrics
import android.util.TypedValue



class AnimationUtils(resources: Resources) {

    val dpm:DisplayMetrics
    init {
        dpm = resources.displayMetrics
    }

    fun pxToDp(fl: Float):Float {
        return fl / dpm.density
    }

    fun dpToPx(fl: Float):Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            fl,
            dpm
        )
    }

}