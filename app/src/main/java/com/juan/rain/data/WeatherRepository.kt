package com.juan.rain.data

import android.util.Log
import androidx.lifecycle.LiveData
import com.juan.rain.model.DateNHour
import com.juan.rain.model.LatLng
import com.juan.rain.model.Weather
import com.juan.rain.model.toDateNHour
import com.juan.rain.storage.dao.WeatherDao
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import kotlin.concurrent.thread

class WeatherRepository(private val weatherDao: WeatherDao, private val networkRepository: WeatherNetworkRepository) {

    /**
     * Check if room contains that hour in that day and if not ask for it on the weatherNetworkRepo
     * fill the database and return it, ezpz
     */

    fun getWeather(location:LatLng):LiveData<List<Weather>>{
        val dateNHour = LocalDateTime.now().toDateNHour()
        thread {
            val weatherInDB = isWeatherAvailable(dateNHour)
            if (!weatherInDB) {

                networkRepository.weatherRequest(location, object : WeatherCallback {
                    override fun onResponse(weather: List<Weather>?) {
                        if (weather != null) {
                            Log.d(TAG, "data: ${weather}")
                            thread {
                                weatherDao.insertWeather(weather)
                            }

                        }
                        else Log.d(TAG, "emptyList")
                    }

                    override fun onError(error: String) {
                        Log.d(TAG, "getWeatherError: $error")
                    }
                })
            }
        }
        return weatherDao.getWeather(dateNHour.date, dateNHour.hour)
    }

    private fun isWeatherAvailable(dateNHour: DateNHour) =
        weatherDao.checkAvailability(dateNHour.date, dateNHour.hour) != null

    companion object {
        private val TAG = "WeatherRepository"
    }

}

interface WeatherCallback {
    fun onResponse(weather:List<Weather>?)
    fun onError(error:String)
}
