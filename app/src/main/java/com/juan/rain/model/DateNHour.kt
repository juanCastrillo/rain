package com.juan.rain.model

import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime

data class DateNHour(
    var date:LocalDate,
    var hour:Int
)

fun LocalDateTime.toDateNHour() = DateNHour(
    date = this.toLocalDate(),
    hour = this.hour
)