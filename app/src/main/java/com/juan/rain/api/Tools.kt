package com.juan.rain.api


import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator
import com.luckycatlabs.sunrisesunset.dto.Location
import org.threeten.bp.LocalTime
import org.threeten.bp.format.DateTimeFormatter
import java.util.*


class Tools {
    companion object {
        var location: Location = Location("43.468927", "-3.826832")
        var calculator = SunriseSunsetCalculator(location, "Spain/Madrid")

        var formatter = DateTimeFormatter.ofPattern("hh:mm")
        fun sunriseTime():LocalTime = LocalTime.parse(getSunrise(), formatter)

        private fun getSunrise() = calculator.getOfficialSunriseForDate(Calendar.getInstance())

        fun getSunset() = calculator.getOfficialSunsetForDate(Calendar.getInstance())

    }
}