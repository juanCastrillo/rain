package com.juan.rain.utils

import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter

class DateTimeUtils {

    companion object {
        val archiveFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd")

        fun epochToLocalDateTime(datetime: Long) =
            LocalDateTime.ofInstant(Instant.ofEpochSecond(datetime), ZoneId.systemDefault())

    }
}

fun LocalDate.toArchiveString():String = DateTimeUtils.archiveFormatter.format(this)

fun stringToLocalDate(date:String) = LocalDate.parse(date, DateTimeUtils.archiveFormatter)