package com.juan.rain.model

data class LatLng(
    var latitude:Double,
    var longuitude:Double
)